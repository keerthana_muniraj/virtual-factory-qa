#!/bin/bash

protoc -I=proto/ --python_out=./ proto/kuka/api/*.proto
echo > kuka/api/__init__.py
protoc -I=proto/ --python_out=./ proto/kuka/api/core/*.proto
echo > kuka/api/core/__init__.py
protoc -I=proto/ --python_out=./ proto/kuka/api/nav/robot/v1alpha1/*.proto
echo > kuka/api/nav/__init__.py
echo > kuka/api/nav/robot/__init__.py
echo > kuka/api/nav/robot/v1alpha1/__init__.py
protoc -I=proto/ --python_out=./ proto/kuka/api/nav/v1alpha1/*.proto
echo > kuka/api/nav/v1alpha1/__init__.py
protoc -I=proto/ --python_out=./ proto/kuka/api/nav/wom/v1alpha1/*.proto
echo > kuka/api/nav/wom/__init__.py
echo > kuka/api/nav/wom/v1alpha1/__init__.py
protoc -I=proto/ --python_out=./ proto/kuka/api/v1alpha1/*.proto
echo > kuka/api/v1alpha1/__init__.py
protoc -I=proto/ --python_out=./ proto/kuka/api/roboception/*.proto
echo > kuka/api/roboception/__init__.py