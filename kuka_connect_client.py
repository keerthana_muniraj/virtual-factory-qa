#!/usr/bin/python

import collections
import datetime
import gflags
import jwt
import logging
import os
import random
import robust_websocket
import requests
import re


from google.protobuf import json_format

FLAGS = gflags.FLAGS
gflags.DEFINE_string('server', 'www-dev.kuka-atx.com', 'Address of the Websocket server.')
gflags.DEFINE_string('ws_protocol', 'wss', '"ws" or "wss" for insecure or secure WebSockets')
gflags.DEFINE_string('org', 'Dev', 'Which org the robot is in: Dev, Test, Prod NA, Prod EU')

global TIER
try:
    cluster = re.match('name=(\S+)', os.environ['CLUSTER_PROPERTIES']).group(1)
    if 'prod' in cluster:
        TIER = 'www.kuka-atx.com'
    else:
        TIER = 'www-dev.kuka-atx.com'
except KeyError:
    TIER = 'www-dev.kuka-atx.com'

DEFAULT_PONG_TIMEOUT = 15


# build all the urls
def build_fault_url(serial_number, encoding="proto"):
    return 'https://%s/api/faults/%s' % (TIER, serial_number)


def build_data_url(serial_number, encoding="proto"):
    return '%s://%s/api/operationalData/operationalDataIn/%s/%s' % (
            FLAGS.ws_protocol, FLAGS.server, encoding, serial_number)


def build_robot_url(serial_number):
    return 'https://%s/api/devices/%s/properties' % (TIER, serial_number)


def build_maintenance_url(id, encoding="proto"):
    return 'https://%s/api/v1/maintenanceScheduler/milestones/%s/setScheduledDate' % (TIER, id)


def build_create_completed_maintenance_url():
    return "https://%s/api/v1/maintenanceScheduler/milestones" % TIER


def build_delete_maintenance_url(id):
    return 'https://%s/api/v1/maintenanceScheduler/milestones/%s/setScheduledDate' % (TIER, id)


def build_get_maintenance_type_url(serial_number):
    return 'https://%s/api/v1/maintenanceScheduler/devices/%s/types' % (TIER, serial_number)


def build_get_maintenance_events_url(serial_number):
    return "https://%s/api/v1/maintenanceScheduler/milestones?device_id=" % (TIER) + serial_number + "&window_begin=2010-07-27T16%3A28%3A52.032Z&window_end=2024-07-27T16%3A28%3A52.032Z"


def build_append_note_url(id):
    return "https://https://%s/api/v1/maintenanceScheduler/milestones/%s/notes" % (TIER, id)


def build_clear_maintenance_url(id):
    return 'https://%s/api/v1/maintenanceScheduler/milestones/%s/markAsCompleted' % (TIER, id)


def random_token():
    # TODO(charles): Replace with secrets.token_urlsafe(16) once we move to Python 3.
    return str(random.randint(1024 * 1024, 1024 * 1024 * 1024)).encode('base64')


# build payload
def build_jwt(serial_number, private_key):
    now = datetime.datetime.utcnow()
    payload = {
        'exp': now + datetime.timedelta(seconds=180),
        'nbf': now - datetime.timedelta(seconds=60),
        'aud': 'https://www.kuka-atx.com',
        'sub': serial_number,
        'jti': random_token(),
        'iss': 'Python KUKA Connect library',
        'roles': ['device'],
    }

    headers = {
        'kid': 'robot_1'
    }

    return jwt.encode(payload, private_key, algorithm='RS512', headers=headers)


def build_jwtFault(serial_number, private_key):
    now = datetime.datetime.utcnow()
    payload = {
        'exp': now + datetime.timedelta(seconds=180),
        'nbf': now - datetime.timedelta(seconds=60),
        'aud': 'https://www.kuka-atx.com',
        'sub': serial_number,
        'jti': random_token(),
        'iss': 'Python KUKA Connect library',
        'roles': ['uber-admin'],
    }

    headers = {
        'kid': 'robot_1'
    }

    return jwt.encode(payload, private_key, algorithm='RS512', headers=headers)


def build_jwt_maintenance(private_key):
    now = datetime.datetime.utcnow()
    if FLAGS.org == 'Dev':
        org = 'f912bf53-5a3f-4915-8d31-d98312cbfca9'
    elif FLAGS.org == 'Prod NA':
        org = 'f912bf53-5a3f-4915-8d31-d98312cbfca9'
    elif FLAGS.org == 'Prod EU':
        org = '0013E00000ALcXD'
    elif FLAGS.org == 'Test':
        org = '100'

    payload = {
        'exp': now + datetime.timedelta(seconds=180),
        'nbf': now - datetime.timedelta(seconds=60),
        'aud': 'https://www.kuka-atx.com',
        'sub': 'https://connectdev-kukaprod.cs85.force.com/&&https://test.salesforce.com/id/00D6E000000Cz8BUAS/0056E000000XjOeQAK',
        'jti': random_token(),
        'iss': 'Python KUKA Connect library',
        'organization': org,

        # insert org with flag!!!
        'roles': ['uber-admin'],
    }

    headers = {
        'kid': 'robot_1'
    }

    return jwt.encode(payload, private_key, algorithm='RS512', headers=headers)


class KukaConnectClient(object):
    """Client that wraps major KUKA Connect APIs."""

    def __init__(self, as_json=False, pong_timeout=DEFAULT_PONG_TIMEOUT):
        self._fault_ws = collections.defaultdict(lambda: None)
        self._data_ws = collections.defaultdict(lambda: None)
        self._as_json = as_json
        self.pong_timeout = pong_timeout
        self._private_key = ""
        with open('certificates/privatekey.pem') as f:
            self._private_key = f.read()

    def __del__(self):
        self.close()

    def close(self):
        for ws in self._fault_ws.itervalues():
            if ws is not None:
                ws.close()

        for ws in self._data_ws.itervalues():
            if ws is not None:
                ws.close()

        self._fault_ws = collections.defaultdict(lambda: None)
        self._data_ws = collections.defaultdict(lambda: None)

# attempt send and send for op data
    def _attempt_send(self, serial_to_ws, url, message):
        serial_number = message.device_id.serial_number
        ws = serial_to_ws[serial_number]
        if ws is None:
            ws = robust_websocket.RobustWebSocket(pong_timeout=self.pong_timeout)
            headers = ["Authorization: Bearer " + build_jwt(serial_number, self._private_key)]
            logging.info('Connecting to %s at %s', serial_number, url)
            ws.connect(url, headers)
            serial_to_ws[serial_number] = ws

        if ws is None:
            raise Exception("WebSocket not connected.")

        if self._as_json:
            ws.send(json_format.MessageToJson(message))
        else:
            ws.send_binary(message.SerializeToString())

    def _send(self, serial_to_ws, url, message):
        """Send message data."""
        try:
            self._attempt_send(serial_to_ws, url, message)
        except Exception as e:
            logging.warning('First attempt to send message failed: %s', str(e))
            serial_to_ws[message.device_id.serial_number] = None
            try:
                self._attempt_send(serial_to_ws, url, message)
            except Exception as e:
                logging.error('Second and final attempt to send message failed: %s', str(e))
                serial_to_ws[message.device_id.serial_number] = None


# send fault with url and payload above and json from mock robot
    def send_fault(self, fault, snr):
        logging.info("Hitting fault endpoint for {}".format(snr))
        url = build_fault_url(snr, "json")
        try:
            r = requests.post(url, headers={'Authorization': 'Bearer %s' % (build_jwtFault(snr, self._private_key))}, json=fault)
        except Exception as e:
            logging.warning("Fault was not sent" + str(e))

# create an already created maintenance event
    def create_completed_maintenance(self, set):
        url3 = build_create_completed_maintenance_url()
        requests.post(url3,
                      headers={'Authorization': 'Bearer %s' % (build_jwt_maintenance(self._private_key))},
                      json=set)

# schedule a maintenance event
    def schedule_maintenance(self, maintenance, note):
        url = build_maintenance_url(maintenance["id"], "json")
        self.delete_maintenance(maintenance)

        requests.post(url, headers={'Authorization': 'Bearer %s' % (build_jwt_maintenance(self._private_key))},
                      json=maintenance)
        # self.append_note_maintenance(maintenance ,note)

        time = maintenance['scheduled_date_time']
        time = time[:8] + str(int(time[8:10])+5) + time[10:]
        maintenance['scheduled_date_time'] = time

        requests.post(url, headers={'Authorization': 'Bearer %s' % (build_jwt_maintenance(self._private_key))},
                      json=maintenance)

# mark a maintenance event as completed
    def delete_maintenance(self, maintenance):
        url = build_maintenance_url(maintenance["id"], "json")
        requests.delete(url, headers={
            'Authorization': 'Bearer %s' % (build_jwt_maintenance(self._private_key))},
                             json=maintenance['id'])

# get possible types
    def get_maintenance_type(self, snr):
        url = build_get_maintenance_type_url(snr)
        r = requests.get(url, headers={
            'Authorization': 'Bearer %s' % (build_jwt_maintenance(self._private_key))}, json=None)
        return r.text

# get events with id
    def get_maintenance_events(self, snr):
        url = build_get_maintenance_events_url(snr)
        r = requests.get(url, headers={
            'Authorization': 'Bearer %s' % (build_jwt_maintenance(self._private_key))}, json=None)
        return r.text

# append a note
    def append_note_maintenance(self, maintenance, note):
        url = build_append_note_url(maintenance['id'])
        requests.post(url, headers={
            'Authorization': 'Bearer %s' % (build_jwt_maintenance(self._private_key))}, json=note)

# clear a maintenance event
    def clear_maintenance(self, clear):
        url = build_clear_maintenance_url(clear["id"])
        requests.post(url, headers={'Authorization': 'Bearer %s' % (build_jwt_maintenance(self._private_key))},
                      json=clear)

# get robotdata to say the type
    def get_robotdata(self, snr):
        url = build_robot_url(snr)
        logging.info("Querying metadata at {}".format(url))
        r = requests.get(url, headers={'Authorization': 'Bearer %s' % (build_jwtFault(snr, self._private_key))},
                         json=None)
        type = r.text
        try:
            type = re.search('"name":(.+?),"assets"', type).group(1)
        except AttributeError:
            return None
        return type

    def update(self, data):
        if self._as_json:
            url = build_data_url(data.device_id.serial_number, "json")
        else:
            url = build_data_url(data.device_id.serial_number, "proto")

        self._send(self._data_ws, url, data)


# for some reason this returns a 400 error even when the payload is successful via swagger
"""
# post device metadata
    def post_robotdata(self, snr, metadata):
        json_metadata = json_format.MessageToJson(metadata)
        url = build_robot_url(snr)
        r = requests.post(url, headers={'Authorization': 'Bearer %s' % (build_jwtFault(snr, self._private_key))},
                          data=json_metadata)
        return r.status_code
"""
