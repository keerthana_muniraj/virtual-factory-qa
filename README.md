# README #
#Virtual Factory #
### Cloning ###

* git clone --recursive git@bitbucket.org:kukaaustin/virtual-factory.git

### Installing Prerequisites ###

0. Operating system
This has not proven compatible with Ubuntu 18. It was developed on macOS and others have found success with Ubuntu 16.
macOS have issues with authentication(jwt)

1. Install python 2.7
`python 2.7 with all prerequisites is in the folder setuphelp/requirements.txt
`
Some prerequisites:

TODO: update these
* pip install enum34
* sudo apt-get install libgflags-dev / or download here: https://pypi.python.org/pypi/python-gflags/3.1.1
* pip install slackclient
* pip install websocket
* pip install jwt
* pip install six
* pip install protobuf


### Running Locally ###
Note: don't forget to build your protos using the script build_protos.sh 
(make sure you have th submodules before running this script)

For example:

python kuka_test.py --generate_id_numbers=True --starting_id_number=VF0005 --task=playback --period=1  --crash=True --robots=3 --ragnar=True


### Setting up Docker Container (May change when Dockerfile works properly) ###
TODO: get dockerfile working properly

#### 1.  Set all flags in kuka_test.py ####

Due to a currently undiagnosed issue with our Dockerfile, you must ensure all the flags in kuka_test.py
are set to default to the values you want running in your pod.

Please note there is some login in kuka_test.py in the configure_for_tier() method that may overwrite some
options based on if you are running locally, in dev, or in prod.

Here are some examples of the flags you may want to change.

    gflags.DEFINE_string('starting_id_number', 'VF0001', 'If generating id numbers, start from this one.')
    gflags.DEFINE_integer('robots', '4', 'Number of robots to mock.')
    gflags.DEFINE_boolean('ragnar', True, 'if True, Ragnar the cutest Slackbot is activated wuff')
    gflags.DEFINE_boolean('fault', True, "random Faults activate")
    gflags.DEFINE_boolean('maintenance', False, "maintenance events triggered")
    
    gflags.DEFINE_boolean('crash', True, 'Crash robot temperature ocassionally.')
    gflags.DEFINE_boolean('crash_iiwa', False, 'Crash robot temperature ocassionally.')
    gflags.DEFINE_boolean('crash_agilus', True, 'Crash robot temperature ocassionally.')
    gflags.DEFINE_string('bot', 'Dev', 'Select ragnar, gracie,etc bot')


#### 2.  Build you Docker image ####

This script will build your docker image according to the instructions in the Dockerfile.
Depending on what you're doing, you may need to change the Dockerfile. Typically you won't.

    ./docker_build.sh

#### 3.  Push your docker image ####

First change the version in the docker_push.sh script. If you're deploying to dev, use the format
VERSION=${2:-"1.0.0-SNAPSHOT"}. If you're deploying to prod, use VERSION=${2:-"1.0.0"} instead.

Then run the script with:

    ./docker_push.sh

#### 4.  Deploy your docker image ####

The deploy.sh script will use k8smanager to deploy a version of the docker image.
Change the version (ALWAYS use the format VERSION=${2:-"1.0.0"}, regardless if for dev or prod).
Also change the tier (options are TIER=${2:-"www-dev"} for dev and TIER=${2:-"www"} for prod).

    ./deploy.sh

### The different slack bots: ###

    Bot for Dev : ragnar
    Bot for Prod: gracie

    Typically unused:
    Bot for Testorg: raider
    Bot for Local: jacky
    
### Which robots are in use: ###

    Dev : VF0001 - VF0150
    
    Test: I0001 - I0004
    
    Prod: VF0001 - VF0150

### Different files: ###

    kuka-test: calls function

    kuka-connect-client: links are created, requests are made, function are build

    mock-robot: build data and information

    ragnar: controlls slack bot

    nebthing-parser:outread the txt files to give movement data

    flags: controll the happening

### Tips and Tricks: ###
    
 Dockerfile flags don't work, before deployment set default values to the intended behavior!!!!!
    
 For testing anomaly VF0003 and VF0075 and I0003 are set anomalous with ragnar to check whether expected behavior occures.
    
 If some robots don't move or move strange, check the robot type if it does not include "ready", "iiwa", "KR" it won't be recognized and won't move or get the wrong movement data.

For all further questions ask Christina!

There is also a Confluence page with additinal information : https://kuka-atx.atlassian.net/wiki/spaces/QA/pages/83132741/Virtual+Factory
