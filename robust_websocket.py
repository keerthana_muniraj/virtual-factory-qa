import gflags
import logging
import time
import websocket

FLAGS = gflags.FLAGS
gflags.DEFINE_float('ping', 10, 'How often, in seconds, to send ping frames.')
gflags.DEFINE_float('pong_timeout', 15, 'Pong timeout when checking connections.')
gflags.DEFINE_integer('retry', 3, 'Number of connections to attempt before giving up.')


class Error(Exception):
    pass


class PongError(Exception):
    pass


class RobustWebSocket(object):
    """Wrapper around websocket library that tries harder.

    Has logic for automatically retrying failed connections.
    Uses ping/pong control frames to verify connection.
    """

    def __init__(self, pong_timeout=None):
        # Payload of the latest pong, which is the send timestamp in seconds
        # of the corresponding ping.
        self._last_pong = 0

        # Timestamp, in seconds, of last ping sent out.
        self._last_ping = 0
        self._ws = websocket.WebSocket()

        if pong_timeout is not None:
            self.pong_timeout = pong_timeout
        else:
            self.pong_timeout = FLAGS.pong_timeout

    def connect(self, url, headers=[]):
        self._last_ping = time.time()
        self._last_pong = time.time()

        for i in xrange(FLAGS.retry):
            try:
                return self._ws.connect(url, header=headers)
            except Exception as e:
                logging.warning('Unable to connect to %s (attempt %d): %s', url, i, str(e))
                time.sleep(i * 0.2 + 0.1)
        logging.error('Unable to connect to %s after %d attempts', url, FLAGS.retry)

    def keep_alive(self):
        """Manages ping and pong control frames to verify connectivity."""
        if FLAGS.ping > 0:
            timeout = self._ws.timeout
            self._ws.timeout = 0
            try:
                while True:
                    opcode, data = self._ws.recv_data(True)
                    if opcode == websocket.ABNF.OPCODE_PONG:
                        # Read time, in ms, from pong payload.
                        self._last_pong = int(data) / 1000.
                        logging.debug('Received pong')
            except Exception:
                pass

            self._ws.timeout = timeout

            now = time.time()
            if (now - self._last_pong) > (self.pong_timeout + FLAGS.ping):
                raise PongError("Last pong at %0.3f which is more than %0.3f seconds ago." %
                                (self._last_pong, self.pong_timeout + FLAGS.ping))

            if (now - self._last_ping) > FLAGS.ping:
                logging.debug("Sending ping.")
                # Put time, in ms, into ping payload.
                self._ws.ping(str(int(now * 1000)))
                self._last_ping = now

    def send(self, payload):
        self.keep_alive()
        return self._ws.send(payload)

    def send_binary(self, payload):
        self.keep_alive()
        return self._ws.send_binary(payload)

    def close(self, url, headers=[]):
        return self._ws.close()
