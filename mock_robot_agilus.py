import datetime
import gflags
import logging
import math
import random
import string
import time
import re
import kuka_connect_client

# import kuka.api.faults_pb2 as fault_proto
import kuka.api.controller_pb2 as controller_proto
import kuka.api.operational_pb2 as operational_proto

FLAGS = gflags.FLAGS
gflags.DEFINE_boolean('stop_agilus', False, 'Stop robot motion ocassionally.')


def random_string(length, alphabet=string.letters):
    return "".join(
        [random.choice(alphabet) for i in xrange(length)])


CONTROLLER_TYPES = ["KRC2", "KRC4", "KRC4", "KRC4"]
ROBOT_TYPES = [
    "#KR210R2700 PRIME C4 FLR",
    "#KR270R2700_MED_AX C4 FLR",
    "#KR6R2210_2 S C4 FLR ZH210",
    "#KR6R900 C4SR FLR 230",
    "#KR6R900 C4SR FLR",
    "#KR10R900 C4SR FLR",
    "#KR10R1100 C4SR FLR",
]
IP_SUBNETS = ["192.168", "10.10", "10.240", "10.241"]
OPERATING_MODES = [operational_proto.EXT] * 45 + [operational_proto.AUT] * 3 + [operational_proto.T1] + [
    operational_proto.T2]

BATTERY_STATES = [operational_proto.CHARGE_OK] * 45 + [operational_proto.CHARGE_OK_LOW] * 20 + [operational_proto.CHARGE_UNKNOWN] + \
                 [operational_proto.CHARGE_TEST_NOK] + [operational_proto.CHARGE_NOK] + [operational_proto.CHARGE_OFF]

SCREW_FASTENING_PERIOD = 4  # seconds


def random_controller():
    controller = controller_proto.Controller()
    controller.robot.id.serial_number = random_string(
        6, string.digits)
    controller.robot.id.entity_id = (
        "S-1-5-21-" + random_string(9, string.digits) +
        "-" + random_string(9, string.digits) +
        "-" + random_string(9, string.digits))
    controller.robot.type = random.choice(ROBOT_TYPES)

    controller.name = random_string(10)
    controller.type = random.choice(CONTROLLER_TYPES)
    controller.kss_version = ("V8." + str(random.randint(0, 5)) +
                              "." + str(random.randint(0, 32)) +
                              "." + str(random.randint(0, 255)))

    num_packages = random.randint(0, 5)
    for i in xrange(num_packages):
        controller.software.add(
            name=random_string(20),
            description=random_string(120, string.letters + ',. '),
            project_id=random_string(10, string.letters + string.digits + '-')
        )

    controller.cell.id = random_string(10)
    controller.cell.name = random_string(20)
    controller.cell.members = 1

    controller.network.host_name = "WINDOWS-" + random_string(8).upper()
    controller.network.ip_address = (random.choice(IP_SUBNETS) +
                                     "." + str(random.randint(0, 255)) +
                                     "." + str(random.randint(0, 255)))

    return controller


def get_incr_val(curr_val, min, max, increment):
    new_val = min
    if (min < max) and (curr_val <= max):
        new_val = curr_val + increment
        if new_val > max:
            new_val = (new_val % max) + min
    return new_val


def small_random_jump(start, min_val, max_val, scaling_factor=1):
    jump = random.choice([-1, -2, 0, 0, 0, 1, 2]) * scaling_factor
    if (start + jump) > max_val:
        return max_val
    elif (start + jump) < min_val:
        return min_val
    else:
        return start + jump


class MockRobot(object):
    def __init__(self, crash, faults, maint, position_data=None):
        self.controller = random_controller()
        self.data = operational_proto.OperationalData()
        self.data.device_id.CopyFrom(self.controller.robot.id)

        for i in xrange(6):
            self.data.joint.add(name="A" + str(i + 1))

        for joint in self.data.joint:
            joint.position = 0
            joint.temperature = 290
            joint.current = 0
            joint.force = 0

        self.data.mode = random.choice(OPERATING_MODES)
        self.op_modes = [operational_proto.T1, operational_proto.T2, operational_proto.AUT, operational_proto.EXT]
        self.data.energy.energy = 0
        self.data.accumulator_state = random.choice(BATTERY_STATES)
        self.battery_state = [operational_proto.CHARGE_OK, operational_proto.CHARGE_OK_LOW,
                              operational_proto.CHARGE_UNKNOWN,
                              operational_proto.CHARGE_TEST_NOK, operational_proto.CHARGE_NOK,
                              operational_proto.CHARGE_OFF]

        self._stopped = FLAGS.stop and random.random() > 0.8

        self.faults = []
        for i in xrange(0, 20):
            self.faults.append(None)

        self.maintenance = []
        for i in xrange(0, 5):
            self.maintenance.append(None)

        self.fault_status = False
        self.fault_times = {}
        self.update_count = 0

        self.start_time = time.time()

        self.position_data = position_data

        self.crash = crash
        self.randomfaults = faults
        self.set_maintenance = maint

        self.data.controller_cpu_load_percent.value = 50
        self.data.controller_memory_use_megabytes.value = 4000

    # ragnar:
    def set_op_mode(self, mode):
        if mode in xrange(0, 4):
            self.data.mode = self.op_modes[mode]
            return mode
        else:
            return None

    def get_op_mode(self):
        if self.data.mode == operational_proto.T1:
            return 0
        elif self.data.mode == operational_proto.T2:
            return 1
        elif self.data.mode == operational_proto.AUT:
            return 2
        elif self.data.mode == operational_proto.EXT:
            return 3

    def set_battery_state(self, mode):
        if mode in xrange(0, 6):

            self.data.accumulator_state = self.battery_state[mode]
            return mode
        else:
            return None

    def get_battery_state(self):
        if self.data.accumulator_state == operational_proto.CHARGE_OK:
            return 0
        elif self.data.accumulator_state == operational_proto.CHARGE_OK_LOW:
            return 1
        elif self.data.accumulator_state == operational_proto.CHARGE_UNKNOWN:
            return 2
        elif self.data.accumulator_state == operational_proto.CHARGE_TEST_NOK:
            return 3
        elif self.data.accumulator_state == operational_proto.CHARGE_NOK:
            return 4
        elif self.data.accumulator_state == operational_proto.CHARGE_OFF:
            return 5

    def set_serial_number(self, serial_number):
        self.data.device_id.serial_number = serial_number
        self.controller.robot.id.serial_number = serial_number

    def set_entity_id(self, entity_id):
        self.data.device_id.entity_id = entity_id
        self.controller.robot.id.entity_id = entity_id

    def set_device_id(self, serial_number, entity_id):
        self.set_serial_number(serial_number)
        self.set_entity_id(entity_id)

    def faultgenerator(self):
        if self.fault_status:

            if random.random() < 0.1:
                fault = None
                for i in xrange(1, 19):
                    if self.faults[i] is not None:
                        fault = self.generate_fault_clear(self.faults[i])
                        self.faults[i] = None
                        break
                self.fault_status = False
                self._stopped = False
                return fault
            else:
                return None
        else:
            if random.random() > 0.95:
                self.fault_status = True
                self._stopped = True
                return self.generate_fault()

            else:
                return None

    def faultinator(self, faultnumber):
        self.fault_status = True
        self._stopped = True
        return self.generate_fault(faultnumber)

    def defaultinator(self, faultnumber):
        if self.faults[faultnumber] is not None:
            fault = self.generate_fault_clear(self.faults[faultnumber])
            self.faults[faultnumber] = None
            self.fault_status = False
            self._stopped = False

            return fault
        else:
            return None

    def generate_fault_clear(self, fault):
        timestamp = datetime.datetime.utcnow().isoformat()
        fault['isCleared'] = True
        fault["timeStamp"] = timestamp[:-3] + "Z"
        return fault

    def generate_fault(self, faultselector=0):
        if faultselector == 0:
            faultselector = random.randint(1, 19)
        timestamp = datetime.datetime.utcnow().isoformat()

        fault = {}
        fault["deviceId"] = str(self.data.device_id.serial_number)
        fault["isCleared"] = False
        fault["timeStamp"] = timestamp[:-3] + "Z"
        fault["handle"] = 12345678
        fault["isCritical"] = True

        if faultselector == 1:
            fault["faultCode"] = "KSS00001"
            fault["description"] = "EMERGENCY STOP"
            fault["faultType"] = "State"

        elif faultselector == 2:
            fault["faultCode"] = "KSS12016"
            fault["description"] = "Operator safety open"
            fault["faultType"] = "State"

        elif faultselector == 3:
            fault["faultCode"] = "KSS00203"
            fault["description"] = "General motion enable"
            fault["faultType"] = "State"

        elif faultselector == 4:
            fault["faultCode"] = "KSS01210"
            fault["description"] = "Ackn. General motion enable"
            fault["type"] = "Quit"

        elif faultselector == 5:
            fault["faultCode"] = "KSS03186"
            fault["description"] = "Drives have been discharged (reason: SAFETY)"
            fault["faultType"] = "State"

        elif faultselector == 6:
            fault["faultCode"] = "KSS00108"
            fault["description"] = "Dynamic braking active"
            fault["faultType"] = "State"

        elif faultselector == 7:
            fault["faultCode"] = "KSS12017"
            fault["description"] = "Operator safety not acknowledged"
            fault["faultType"] = "State"

        elif faultselector == 8:
            fault["faultCode"] = "Simulate3"
            fault["description"] = "Wait for $TIMER_FLAG[3]"
            fault["faultType"] = "Wait"
            
        elif faultselector == 9:
            fault["faultCode"] = "Simulate3"
            fault["description"] = "Wait for ((Input5) or ($TIMER_FLAG[10]>0))"
            fault["faultType"] = "Wait"

        elif faultselector >= 10 and faultselector <= 12:
            fault["faultCode"] = "<ServoGunMessages>215"
            fault["description"] = "Error, block selection or reset required!"
            fault["faultType"] = "Quit"

        elif faultselector == 13:
            fault["faultCode"] = "<StackingAnalog>27"
            fault["description"] = "Measured value is greater than max."
            fault["faultType"] = "Dialog"

        elif faultselector == 14:
            fault["faultCode"] = "<Gripper>903"
            fault["description"] = "Gripper 3: What condition do you want to change?"
            fault["faultType"] = "Dialog"

        elif faultselector == 15:
            fault["faultCode"] = "<WeldError>1"
            fault["description"] = "Violation of the absolute upper tolerance band! Repeat point?"
            fault["faultType"] = "Dialog"

        elif faultselector == 16:
            fault["faultCode"] = "UserKey6"
            fault["description"] = "The logged-on user switched from Expert to Operator."
            fault["faultType"] = "Info"

        elif faultselector == 17:
            fault["faultCode"] = "KSS27012"
            fault["description"] = "Brake test successful"
            fault["faultType"] = "Info"

        elif faultselector == 18:
            fault["faultCode"] = "KSS27009"
            fault["description"] = "Brake A4 OK"
            fault["faultType"] = "Info"

        elif faultselector == 19:
            fault["faultCode"] = "KSS27009"
            fault["description"] = "Brake A2 OK"
            fault["faultType"] = "Info"

        self.faults[faultselector] = fault
        logging.info("Generating fault {} for {}".format(fault["faultCode"], self.data.device_id.serial_number))
        return fault

    def generate_controller(self):
        if random.random() > 0.999 and self.controller.software:
            logging.debug("Removing software package to %s", self.controller.robot.id.entity_id)
            self.controller.software.remove(random.choice(self.controller.software))

        if random.random() > 0.9999:
            logging.debug("Switching IP of %s", self.controller.robot.id.entity_id)
            self.controller.network.ip_address = (random.choice(IP_SUBNETS) +
                                                  "." + str(random.randint(0, 255)) +
                                                  "." + str(random.randint(0, 255)))

        if random.random() > 0.999:
            logging.debug("Adding software package to %s", self.controller.robot.id.entity_id)
            self.controller.software.add(
                name=random_string(20),
                description=random_string(120, string.letters + ',. '),
                project_id=random_string(10, string.letters + string.digits + '-'))

        self.update_count += 1
        return self.controller

    def generate_cleared_maintenance(self, client, private_key, setselector=0):
        timestamp = datetime.datetime.utcnow().isoformat()

        set = {}
        types = client.get_maintenance_type(self.data.device_id.serial_number)
        types = types[9:-2]
        type = types.split(',')
        typeAll = []
        for typ in type:
            typeAll.append(typ)
        if setselector == 0:
            setselector = random.randint(1, len(typeAll))

        set["auth_token"] = kuka_connect_client.build_jwt_maintenance(private_key)
        set["device_id"] = str(self.data.device_id.serial_number)
        set["run_time_in_seconds"] = 14
        set["completed_date_time"] = timestamp[:-3] + "Z"
        set["completed_by_user_id"] = "Some"

        type = typeAll[setselector-1]
        set["type"] = str(type[1:-1])
        return set

    def create_note(self):
        note = {"content": "Worked as expected",
                "composition_by_user_id": "Some"}
        return note

    def clear_maintenance(self, client, private_key, a):
        clear = {}
        timestamp = datetime.datetime.utcnow().isoformat()

        types = client.get_maintenance_type(self.data.device_id.serial_number)
        types = types[9:-2]
        type = types.split(',')
        typeAll = []
        for typ in type:
            typeAll.append(typ)

        ids = []
        milestones = client.get_maintenance_events(self.data.device_id.serial_number)
        milestones = milestones.split('}')
        for k in typeAll:
            for milestone in milestones:
                if k in milestone:
                    id = re.search('"id":(.+?),"device_id"', milestone).group(1)
                    ids.append(id)
                    break

        id = ids[a]
        clear["auth_token"] = kuka_connect_client.build_jwt_maintenance(private_key)
        clear["id"] = str(id[1:-1])
        clear["device_id"] = str(self.data.device_id.serial_number)
        clear["run_time_in_seconds"] = 16
        clear["scheduled_date_time"] = timestamp[:-3] + "Z"
        clear["scheduled_by_user_id"] = "Some"
        return clear

    def generate_maintenance(self, client, private_key, maintenanceselector=0):
        timestamp = datetime.datetime.utcnow().isoformat()

        maintenance = {}
        types = client.get_maintenance_type(self.data.device_id.serial_number)
        types = types[9:-3]
        type = types.split(',')
        typeAll = []
        for typ in type:
            typeAll.append(typ)

        ids = []
        milestones = client.get_maintenance_events(self.data.device_id.serial_number)
        milestones = milestones.split('}')
        for k in typeAll:
            for milestone in milestones:
                if k in milestone:
                    id = re.search('"id":(.+?),"device_id"', milestone).group(1)
                    ids.append(id)
                    break

        if maintenanceselector == 0:
            maintenanceselector = random.randint(1, len(typeAll))
        maintenance["scheduled_by_user_id"] = "Some"
        maintenance["scheduled_date_time"] = timestamp[:-3] + "Z"
        maintenance["auth_token"] = kuka_connect_client.build_jwt_maintenance(private_key)
        maintenance["device_id"] = str(self.data.device_id.serial_number)
        id = ids[maintenanceselector-1]
        type = typeAll[maintenanceselector - 1]
        maintenance["id"] = str(id[1:-1])
        maintenance["type"] = str(type[1:-1])
        return maintenance

    def generate_data(self, period):

        self.data.controller_cpu_load_percent.value = small_random_jump(start=self.data.controller_cpu_load_percent.value, min_val=0, max_val=100)
        self.data.controller_memory_use_megabytes.value = small_random_jump(
                start=self.data.controller_memory_use_megabytes.value,
                min_val=100,
                max_val=8000,
                scaling_factor=10)

        now = time.time()
        self.data.time.seconds = int(now)
        self.data.time.nanos = int((now - self.data.time.seconds) * 10 ** 9)

        up_time = now - self.start_time

        self.data.up_time.seconds = int(up_time)
        self.data.up_time.nanos = int((up_time - self.data.up_time.seconds) * 10 ** 9)
        date = datetime.datetime.utcnow().day
        month = datetime.datetime.utcnow().month

        if not self._stopped:
            for idx, joint in enumerate(self.data.joint):
                if self.position_data:
                    joint.position = self.position_data[self.update_count % len(self.position_data)][idx]

                else:
                    joint.position = math.sin((self.update_count + 6 * idx) / 20.0)

                if self.crash and (month % 2) == 1 \
                        and ((0 <= date <= 9 and idx == 0)
                             or (2 <= date <= 11 and idx == 1)
                             or (4 <= date <= 13 and idx == 2)
                             or (6 <= date <= 15 and idx == 3)
                             or (8 <= date <= 17 and idx == 4)
                             or (10 <= date <= 19 and idx == 5)):
                    if random.random() > 0.99:
                        if joint.temperature <= 320:
                            joint.temperature = joint.temperature + 4
                        elif joint.temperature <= 353 and joint.temperature >= 320:
                            joint.temperature = random.choice(
                                [joint.temperature + 1, joint.temperature - 1, joint.temperature + 0.5,
                                 joint.temperature + 0.5,
                                 joint.temperature + 0.2, joint.temperature - 0.2])
                        elif joint.temperature >= 353:
                            joint.temperature = joint.temperature - 3
                else:
                    if random.random() > 0.99:
                        if joint.temperature <= 295:
                            joint.temperature = joint.temperature + 3
                        elif joint.temperature <= 324 and joint.temperature >= 295:
                            joint.temperature = random.choice(
                                [joint.temperature + 1, joint.temperature - 1, joint.temperature - 0.5,
                                 joint.temperature + 0.5,
                                 joint.temperature + 0.2, joint.temperature - 0.2])
                        elif joint.temperature >= 324:
                            joint.temperature = joint.temperature - 3
                if self.crash and (month % 2 == 1) \
                        and ((12 <= date <= 21 and idx == 0)
                             or (14 <= date <= 23 and idx == 1)
                             or (16 <= date <= 25 and idx == 2)
                             or (18 <= date <= 27 and idx == 3)
                             or (20 <= date <= 29 and idx == 4)
                             or (22 <= date <= 31 and idx == 5)):
                    if joint.current <= 0.55:
                        joint.current = joint.current + 0.1
                    elif joint.current <= 1.5 and joint.current >= 0.48:
                        joint.current = random.choice(
                            [joint.current + 0.01, joint.current - 0.01, joint.current - 0.02,
                             joint.current + 0.02,
                             joint.current + 0.05, joint.current - 0.05])
                    elif joint.current >= 1.5:
                        joint.current = joint.current - 0.1
                else:
                    if joint.current <= -0.55:
                        joint.current = joint.current + 0.1
                    elif joint.current <= 0.48 and joint.current >= -0.48:
                        joint.current = random.choice(
                            [joint.current + 0.01, joint.current - 0.01, joint.current - 0.02,
                             joint.current + 0.02,
                             joint.current + 0.05, joint.current - 0.05])
                    elif joint.current >= 0.55:
                        joint.current = joint.current - 0.1

                if self.crash and month % 2 == 0 \
                        and ((0 <= date <= 12 and idx == 0)
                             or (4 <= date <= 16 and idx == 1)
                             or (8 <= date <= 20 and idx == 2)
                             or (12 <= date <= 24 and idx == 3)
                             or (16 <= date <= 28 and idx == 4)
                             or (20 <= date <= 32 and idx == 5)):
                    if joint.force <= 15:
                        joint.force = joint.force + 1
                    elif joint.force <= 11 and joint.force >= 15:
                        joint.force = random.choice(
                            [joint.force + 0.01, joint.force - 0.01, joint.force - 0.05,
                             joint.force + 0.05,
                             joint.force + 0.02, joint.force - 0.02])
                    elif joint.force >= 11:
                        joint.force = joint.force - 1
                else:
                    if joint.force <= -12:
                        joint.force = joint.force + 1
                    elif joint.force <= 12 and joint.force >= -12:
                        joint.force = random.choice(
                            [joint.force + 0.01, joint.force - 0.01, joint.force - 0.05,
                             joint.force + 0.05,
                             joint.force + 0.02, joint.force - 0.02])
                    elif joint.force >= 12:
                        joint.force = joint.force - 1

                if self.data.energy.energy <= 0:
                        self.data.energy.energy = self.data.energy.energy + 0.00001
                elif self.data.energy.energy <= 1 * 0.00001 and self.data.energy.energy >= 0:
                        self.data.energy.energy = random.choice(
                            [self.data.energy.energy + 0.00001, self.data.energy.energy - 0.000001, self.data.energy.energy - 0.000005,
                             self.data.energy.energy + 0.000005,
                             self.data.energy.energy + 0.000002, joint.current - 0.000002])
                elif self.data.energy.energy >= 1 * 0.00001:
                        self.data.energy.energy = self.data.energy.energy - 0.000001

        else:
            for idx, joint in enumerate(self.data.joint):
                if random.random() > 0.98:
                    joint.temperature = max(290, joint.temperature - 1)
                    self.data.energy.energy = self.data.energy.energy - 0.000001
                    joint.force = max(0.2, joint.force - 0.01)
                    joint.current = max(0.03, joint.current - 0.0001)
            # This corresponds to 9 watts on average.
            # (0.000005 kilowatt hours) / second = 18 watts
            self.data.energy.energy = self.data.energy.energy - 0.01

        if FLAGS.stop and random.random() > 0.999:
            logging.debug("Starting/stopping %s", self.controller.robot.id.entity_id)
            self._stopped = not self._stopped

        if random.random() > 0.997:
            logging.debug("Switching mode.")
            self.data.mode = random.choice(OPERATING_MODES)

        if random.random() > 0.997:
            logging.debug("Change AccuState")
            self.data.accumulator_state = random.choice(BATTERY_STATES)

        self.data.energy.duration.seconds += 1
        if FLAGS.counter is False:
            self.update_count += 1

        # this should probably be better. sorry :(  -christina
        self.data.run_time.seconds = self.data.up_time.seconds + 123456
        return self.data
