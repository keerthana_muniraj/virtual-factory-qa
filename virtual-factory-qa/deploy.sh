#!/usr/bin/env bash

# Usage: ./deploy_service <version> <tier>

VERSION=${1:-"1.0.6"}
TIER=${2:-"www-dev"}

curl --anyauth -u admin:!skcus1N -X POST "https://$TIER.kuka-atx.com/api/k8smanager/services/mock-robot/$VERSION"
