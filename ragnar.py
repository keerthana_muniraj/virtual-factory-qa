import time
import random
from slackclient import SlackClient


class Ragnar(object):
    def __init__(self, robots, robots_i, robots_a, client, bot):
        # starterbot's ID as an environment variable
        self.BOT_ID = "U64S2E3H6"

        # constants
        self.AT_BOT = "<@" + ">"
        # + self.BOT_ID
        if bot == 'Dev':
            self.slack_client = SlackClient("xoxb-11207041587-428677466611-xGJomyiGPiKHdMVf6CBEGFlL")
        elif bot == 'Prod':
            self.slack_client = SlackClient("xoxb-239132729011-z2bJ8qb1NrbKPMmiFrJx4P7q")
        elif bot == 'Test':
            self.slack_client = SlackClient("xoxb-227146071157-wZEBtuMV3ZWm3P2xv31upqX8")
        elif bot == 'Playground':
            self.slack_client = SlackClient("xoxb-231725869783-mI0oDLau7tTfgm2Tyysl7lei")

        self.bot = bot
        self.client = client
        self.robots = robots
        self.robots_a = robots_a
        self.robots_i = robots_i
        self.ragnar_news = False
        self.ragnar_fault = 0
        self.ragnar_robot = ""
        self.ragnar_attack = False

        self.channels = []
        self.READ_WEBSOCKET_DELAY = 1  # 1 second delay between reading from firehose
        if self.slack_client.rtm_connect():
            print("wuff wuff ragnar active")

    def doit(self):
        command, channel = self.parse_slack_output(self.slack_client.rtm_read())

        if command and channel:
            self.handle_command(command, channel)

    def handle_command(self, command, user_channel):
        """
            Receives commands directed at the bot and determines if they
            are valid commands. If so, then acts on the commands. If not,
            returns back what it needs for clarification.
        """
        channel = None
        for achannel in self.channels:
            if achannel.id == user_channel:
                channel = achannel
                break
        if channel is None:
            channel = Channel(user_channel)
            self.channels.append(channel)
            self.slack_client.api_call("chat.postMessage",
                                       channel=channel.id,
                                       text="Hello,\n\
                                             type *robots* to see all robots under my control\n\
                                             type a serial number to access one robot\n\
                                             if you are a pro you can try *serial number* + *space* + *command*",
                                       as_user=True)
        response = "wuff wrong command, try *robots* and end all your commands with an !"

        if channel.state == 0:
            if command.startswith("help"):
                channel.validcommand = True

            if command.startswith("robots"):
                channel.validcommand = True
                response = "Choose one of the following robots:\n"
                response = response + "R2F Robots: \n"
                for robot in self.robots:
                    response = response + robot.data.device_id.serial_number + "\n"
                response = response + "\nAgilus Robots: \n"
                for robot in self.robots_a:
                    response = response + robot.data.device_id.serial_number + "\n"
                response = response + "\niiwa Robots: \n"
                for robot in self.robots_i:
                    response = response + robot.data.device_id.serial_number + "\n"
            for robot in self.robots:
                if robot.data.device_id.serial_number in command[:len(robot.data.device_id.serial_number)]:
                    channel.robot = robot
                    if command[len(robot.data.device_id.serial_number)+1:] == "":
                        channel.state = 1
                    else:
                        response = channel.robotsubmenu(command[len(robot.data.device_id.serial_number)+1:], self.client)
            for robot in self.robots_a:
                if robot.data.device_id.serial_number in command[:len(robot.data.device_id.serial_number)]:
                    channel.robot = robot
                    if command[len(robot.data.device_id.serial_number)+1:] == "":
                        channel.state = 2
                    else:
                        response = channel.robotsubmenu(command[len(robot.data.device_id.serial_number)+1:], self.client)
            for robot in self.robots_i:
                if robot.data.device_id.serial_number in command[:len(robot.data.device_id.serial_number)]:
                    channel.robot = robot
                    if command[len(robot.data.device_id.serial_number)+1:] == "":
                        channel.state = 3
                    else:
                        response = channel.robotsubmenu(command[len(robot.data.device_id.serial_number)+1:], self.client)

            if channel.state in xrange(1, 4):
                channel.validcommand = True

                response = "You have chosen "
                if channel.state == 1:
                    response = response + "a R2F "
                    faults = "1. [State] KSS00001 EMERGENCY STOP\n\
                              2. [State] KSS12016 Operator safety open\n\
                              3. [State] KSS00203 General motion enable\n\
                              4. [State] KSS01210 Ackn. General motion enable\n\
                              5. [State] KSS03186 Drives have been discharged (reason: SAFETY)\n\
                              6. [State] KSS00108 Dynamic braking active\n\
                              7. [State] KSS12017 Operator safety not acknowledged\n\
                              8. [Wait] Simulate3 Wait for $TIMER_FLAG[7]\n\
                              9. [Wait] Simulate3 Wait for (($TIMER_FLAG[1]>0) or Input4)\n\
                              10. [Info] UserKey6 The logged-on user switched from Expert to Operator.\n\
                              11. [Info] KSS27012 Brake test successful\n\
                              12. [Info] KSS27009 Brake A4 OK\n\
                              13. [Info] KSS27009 Brake A2 OK\n"
                if channel.state == 2:
                    response = response + "an Agilus "
                    faults = "1. [State] KSS00001 EMERGENCY STOP\n\
                              2. [State] KSS12016 Operator safety open\n\
                              3. [State] KSS00203 General motion enable\n\
                              4. [Quit] KSS01210 Ackn. General motion enable\n\
                              5. [State] KSS03186 Drives have been discharged (reason: SAFETY)\n\
                              6. [State] KSS00108 Dynamic braking active\n\
                              7. [State] KSS12017 Operator safety not acknowledged\n\
                              8. [Wait] for $TIMER_FLAG[3]\n\
                              9. [Wait] Simulate3 Wait for ((Input5) or ($TIMER_FLAG[10]>0))\n\
                              10, 11, 12. [Quit] <ServoGunMessages>215 Error, block selection or reset required!\n\
                              13. [Dialog] <StackingAnalog>27 Measured value is greater than max.\n\
                              14. [Dialog] <Gripper>903 Gripper 3: What condition do you want to change?\n\
                              15. [Dialog] <WeldError>1 Violation of the absolute upper tolerance band! Repeat point?\n\
                              16. [Info] UserKey6 The logged-on user switched from Expert to Operator.\n\
                              17. [Info] KSS27012 Brake test successful\n\
                              18. [Info] KSS27009 Brake A4 OK\n\
                              19. [Info] KSS27009 Brake A2 OK\n"
                if channel.state == 3:
                    response = response + "an Iiwa "
                    faults = "1. [State] Sunrise.Error Emergency stop local is active.\n\
                              2. [State] Sunrise.Error Collision has been detected.\n\
                              3. [State] Sunrise.Error Simultaneous use of smartPAD and handguiding device is not allowed.\n\
                              4. [State] Sunrise.Error Error in safe device occured. Safety control is paused.\n\
                              5. [State] Sunrise.Error Encoder error torque sensor. ([K1: A5])\n\
                              6. [Wait] Simulate3 Wait for Input7 or Input2\n\
                              7. [Wait] Simulate3 Wait for Input5\n\
                              8. [Dialog] <Gripper>903 Gripper: What condition do you want to change?\n\
                              9. [Info] UserKey6 The logged-on user switched from Expert to Operator.\n"
                response = response + "Robot.\n"

                response = response + "\nBattery State is set to "

                rob_mode = channel.robot.get_battery_state()
                if rob_mode == 0:
                    response = response + "OK\n"
                elif rob_mode == 1:
                    response = response + "OK LOW\n"
                elif rob_mode == 2:
                    response = response + "UNKNOWN\n"
                elif rob_mode == 3:
                    response = response + "TEST NOK\n"
                elif rob_mode == 4:
                    response = response + "NOK\n"
                elif rob_mode == 5:
                    response = response + "OFF\n"
                else:
                    response = response + "error occured \n"

                response = response + "Operation mode is set to "
                rob_mode = channel.robot.get_op_mode()
                if rob_mode == 0:
                    response = response + "T1\n"
                elif rob_mode == 1:
                    response = response + "T2\n"
                elif rob_mode == 2:
                    response = response + "AUT\n"
                elif rob_mode == 3:
                    response = response + "EXT\n"
                else:
                    response = response + "error occured \n"

                if channel.robot.randomfaults:
                    response = response + "The robot is getting random faults.\n"
                else:
                    response = response + "The robot is getting no random faults.\n"

                if channel.robot.crash:
                    response = response + "Robot is set to an anomalous behavior.\n\n"
                else:
                    response = response + "Robot is behaving normally.\n\n"

                response = response + "You can choose between the following Faults:\n"
                response = response + faults
                response = response + "\nYou can also generate maintenance events.\n"
                response = response + "You can provoke an anomaly.\n"
                response = response + "You can change the OP mode.\n"
                response = response + "You can change the Battery State.\n"

                response = response + "\nPlease Answer:\n*f* + faultnumber to create a fault (0 for random)\n" \
                                      "*c* + faultnumber to clear a fault\n*m* for a maintenance event\n" \
                                      "*fr* + 1 or 0 to switch between random Faults activated\n" \
                                      "*a* + 1 or 0 to switch between anomalous behavior\n" \
                                      "*op* + 0 for T1, 1 for T2, 2 for AUT, 3 for EXT operation mode\n" \
                                      "*b* + 0 OK, 1 OK LOW, 2 UNKNOWN, 3 TEST NOK, 4 NOK, 5 OFF\n" \
                                      "*r* to return"
                channel.state = 4

            # Stuff for fun
            if command.startswith("good boy"):
                if self.bot == "Dev":
                    channel.validcommand = True
                    response = 'wuff wuff wuff wuff :)'
                    channel.state = 6

        elif channel.state == 4:
            response = channel.robotsubmenu(command, self.client)

        elif channel.state == 6:
            response = channel.easteregg(command, self.slack_client)
        else:
                print "wuuuuf"
                channel.state = 0

        if channel.validcommand is True:
            self.slack_client.api_call("chat.postMessage", channel=channel.id, text=response, as_user=True)
            channel.validcommand = False

    def parse_slack_output(self, slack_rtm_output):
        """
            The Slack Real Time Messaging API is an events firehose.
            this parsing function returns None unless a message is
            directed at the Bot, based on its ID.
        """
        output_list = slack_rtm_output
        if output_list and len(output_list) > 0:
            for output in output_list:
                if output and 'text' in output:
                    # return text after the @ mention, whitespace removed
                    return output['text'], \
                           output['channel']
        return None, None


class Channel:
    def __init__(self, id):
        self.id = id
        self.state = 0
        self.robot = None
        self.validcommand = False
        self.help = False
        self.thecount = 15

    def robotsubmenu(self, command, client):
        response = "wrong command,\n try *f*,*c* or *r* "

        # fault
        if command.startswith("f"):
            if command[1] != "r":
                self.validcommand = True
                try:
                    if command[1].isdigit() and command[2].isdigit():
                        faultnr = int(command[1])*10 + int(command[2])
                    else:
                        faultnr = int(command[1])
                except:
                    faultnr = 0
                fault = self.robot.faultinator(faultnr)
                client.send_fault(fault, self.robot.data.device_id.serial_number)
                response = "created fault: " + fault["faultCode"] + " " + fault["description"] + " for Robot: " + fault[
                    "deviceId"]
                self.state = 0
        if command.startswith("c"):
            self.validcommand = True
            try:
                faultnr = int(command[1])
            except:
                faultnr = 0
            fault = self.robot.defaultinator(faultnr)
            client.send_fault(fault, self.robot.data.device_id.serial_number)
            if fault is not None:
                response = "cleared fault for Robot: " + fault["deviceId"]
            else:
                response = "cleared fault"
            self.state = 0

        # anomaly
        if command.startswith("a"):
            self.validcommand = True
            try:
                anom = int(command[1])
            except:
                anom = 0
            if anom == 1:
                self.robot.crash = True
                response = "Robot is now behaving anomalous"
            else:
                self.robot.crash = False
                response = "Robot is now behaving normally"
            self.state = 0

        # maintenance
        if command.startswith("m"):
            self.validcommand = True
            self.robot.set_maintenance = True
            response = "Robot has scheduled maintenance events"

            self.state = 0

        # random faults
        if command.startswith("fr"):
            self.validcommand = True
            try:
                anom = int(command[2])
            except:
                anom = 0
            if anom == 1:
                self.robot.randomfaults = True
                response = "Robot is now getting random faults"
            else:
                self.robot.randomfaults = False
                response = "Robot is now not getting random faults"
            self.state = 0

        # op mode
        if command.startswith("op"):
            self.validcommand = True
            try:
                anom = int(command[2])
            except:
                anom = 10
            re = self.robot.set_op_mode(anom)
            rest = ["T1", "T2", "Aut", "Ext"]
            if re is not None:
                response = "Robot operation mode is now set to " + rest[re]
            else:
                response = "unable to parse operation mode"
            self.state = 0

        # battery state
        if command.startswith("b"):
            self.validcommand = True
            try:
                anom = int(command[1])
            except:
                anom = 10
            re = self.robot.set_battery_state(anom)
            rest = ["OK", "OK LOW", "UNKNOWN", "TEST NOK", "NOK", "OFF"]
            if re is not None:
                response = "Robot battery state is now set to " + rest[re]
            else:
                response = "unable to parse battery state"
            self.state = 0

        # main menu
        if command.startswith("r"):
            self.validcommand = True
            response = "back to main menu wuff"
            self.state = 0
        return response

    def easteregg(self, command, slack_client):
        response = ""
        if self.thecount == 0:
            self.supereasteregg(slack_client)
            self.thecount = 15
        else:
            self.thecount = self.thecount - 1

        if command.startswith("fetch"):
            self.validcommand = True
            response = 'wuff wuff\n *ragnar runs away*'
            slack_client.api_call("chat.postMessage", channel=self.id,
                                  text=response, as_user=True)
            time.sleep(5)
            response = '*ragnar brings back ' + command[6:] + '*'
        elif command.startswith("good boy"):
            self.validcommand = True
            response = '*wuff* *wuff* :)'
        elif command.startswith("bad boy"):
            self.validcommand = True
            response = '*grrrrrrrr*'
        elif command.startswith("ragnar answer"):
            self.validcommand = True
            response = self.eightball()
        elif command.startswith("what would"):
            self.validcommand = True
            response = self.ww()
        elif command.startswith("connyun"):
            self.validcommand = True
            response = 'Ragnar cannot see anything orignial about it'
        elif command.startswith("return"):
            self.validcommand = True
            response = "back to main menu wuff"
            self.state = 0
        print response
        return response

    def eightball(self):
        return "Ragnar thinks " + random.choice(["yes", "wuff", "wuff wuff", "Nein", " it is certain",
                                                 "it is decidedly so", "without a doubt", "yes definitely",
                                                 "you may rely on it", "perhaps", "most likely", "the outlook is good",
                                                 "the signs point to yes", "you should ask again later",
                                                 "he better not tell you now", "he cannot predict now",
                                                 "Don't count on it", "his sources say no",
                                                 "outlook not so good", "very doubtful",
                                                 "there are many dogs but only one Ragnar", "what if we can't walk through mirrors because our reflection is in the way",
                                                 "what if the brain is a parasite and our bodies are just the host",
                                                 "it will happen when the moon is full again", "you should be nice to robots because they soon will rule the world",
                                                 "the deep learning team is actully developing skynet", "Kafka is made by aliens to accelerate our tech progress",
                                                 "there really should be a KUKA Compute product", "if you can eat it, yes"])

    def ww(self):
        return "Ragnar would " + random.choice(["eat it whole", "lick it", "pee on it", "look at it interested", "take a bite",
                                                "chase his own tail", "run around", "yes definitely",
                                                "be attracted by it", "be scared", "look cute", "sleep",
                                                "roll on his back", "mistake cables for food",
                                                "hotfix it to prod", "skip the testing and just do it",
                                                "not tell anybody about it", "get rid of all the witnesses",
                                                "snorr", "bark at it and then run away",
                                                "sneeze and look even cuter"])

    def supereasteregg(self, slack_client):
        ran = random.random()
        if ran < 0.05:
            response = "oh oh Ragnar left a little present "
            ran2 = random.random()
            if ran2 < 0.2:
                response = response + "in the elephant room"
            elif ran2 < 0.4:
                response = response + "behind iiwa"
            elif ran2 < 0.6:
                response = response + "in the yoga room"
            elif ran2 < 0.8:
                response = response + "under the sink"
            else:
                response = response + "in the server room"
        elif ran < 0.2:
            response = "*squeak* *squeak* oh look Ragnar plays with his little toy\n*crunch* that were the cabels"
        elif ran < 0.25:
            response = "*chiing* Ragnar just made himself a bagel"
        elif ran < 0.3:
            response = self.ww()
        elif ran < 0.4:
            response = "*sniff* *sniff* Ragnar is on to a new lead"
        elif ran < 0.48:
            response = self.eightball()
        elif ran < 0.55:
            response = "*clack clack clack* Ragnar is typing on the costume mechanical keyboard he just ordered on Massdrop"
        elif ran < 0.6:
            response = "Ragnar looks suspiciosly "
            ran2 = random.random()
            if ran2 < 0.2:
                response = response + "at the fridge"
            elif ran2 < 0.4:
                response = response + "out off the window"
            elif ran2 < 0.6:
                response = response + "because he ate all the snacks"
            elif ran2 < 0.8:
                response = response + "at all the slacks messages"
            else:
                response = response + "cute"
        elif ran < 0.7:
            response = "*snorrr* Ragnar sleeps in his little bed"
        elif ran < 0.95:
            response = "Ragnar demands "
            ran2 = random.random()
            if ran2 < 0.2:
                response = response + "more soy milk"
            elif ran2 < 0.4:
                response = response + "even more la croix"
            elif ran2 < 0.6:
                response = response + "more happy hours per month"
            elif ran2 < 0.8:
                response = response + "more dog treats"
            else:
                response = response + "more cables to chew"
        else:
            response = "*wuff* *wufff* *wuuufff*"

        slack_client.api_call("chat.postMessage", channel=self.id,
                              text=response, as_user=True)
        return
