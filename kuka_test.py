#!/usr/bin/python

from enum import Enum
import gflags
import logging
import os
import re
import sys
import time
import random

import neb_thing_update_parser as thing_update
import kuka_connect_client
import mock_robot_r2f
import mock_robot_IIWA
import mock_robot_agilus
import ragnar

FLAGS = gflags.FLAGS
gflags.DEFINE_boolean('dry_run', False, "Send message to DEBUG log, not Connect.")
gflags.DEFINE_boolean('json', False, 'Send JSON encoded messages rather than binary proto.')
gflags.DEFINE_float('period', 1.0, 'The period, in seconds, at which to publish operational data.')
gflags.DEFINE_string('console_log_level', 'INFO', 'Logging level to send to console.')
gflags.DEFINE_string('entity_ids', '5,6,7', 'Comma separated list of entity IDs to spoof.')
gflags.DEFINE_string('log_dir', '/tmp/', 'Directory of log file.')
gflags.DEFINE_string('log_level', 'INFO', 'Logging level to send to file.')
gflags.DEFINE_string('serial_numbers', '5,6,7', 'Comma separated list of serial numebers to use.')
gflags.DEFINE_string('task', 'playback', 'Task for the mock robot to perform. Options: sine, playback.')
gflags.DEFINE_boolean('generate_id_numbers', True, 'If True, use incremental serial numbers from starting_id_number and duplicate values to EIDs.')
gflags.DEFINE_string('starting_id_number', '7', 'If generating id numbers, start from this one.')
gflags.DEFINE_integer('robots', '3', 'Number of robots to mock.')
gflags.DEFINE_boolean('ragnar', True, 'if True, Ragnar the cutest Slackbot is activated wuff')
gflags.DEFINE_boolean('fault', True, "random Faults activate")
gflags.DEFINE_boolean('maintenance', False, "maintenance events triggered")
gflags.DEFINE_string('bot', 'Dev', 'Options: Dev, Prod, Test, or Playground')
gflags.DEFINE_boolean('crash', True, 'Crash robot temperature ocassionally.')
gflags.DEFINE_boolean('crash_iiwa', True, 'Crash robot temperature ocassionally.')
gflags.DEFINE_boolean('crash_agilus', False, 'Crash robot temperature ocassionally.')
gflags.DEFINE_boolean('mock_robot', False, 'Publish data for mock robot service instead of mock factory')

"""
To connect to www-dev.kuka-atx.com and publish mock data:
  $ python ./kuka_test.py

To connect to a local server, print DEBUG logs and publish mock data:
  $ python ./kuka_test.py --server localhost:9000 --ws_protocol ws --log_level DEBUG
"""

PONG_TIMEOUT_PER_ROBOT = 0.4  # seconds
ID_REGEX = '([a-zA-Z]+|)(\d+)'
ROBOT_TYPES = ["agilus", "iiwa"]


class Task(Enum):
    SINE = 'sine'
    PLAYBACK = 'playback'


def configure_logging():
    """Configures logging from command line flags."""
    timestr = time.strftime("%Y%m%d_%H%M%S")
    log_path = '%skuka_test_%s.log' % (FLAGS.log_dir, timestr)
    logging.basicConfig(filename=log_path, level=FLAGS.log_level)
    print('Logging %s to %s' % (FLAGS.log_level, log_path))

    # Print to console.
    console = logging.StreamHandler()
    console.setLevel(FLAGS.console_log_level)
    formatter = logging.Formatter('%(levelname)-8s %(message).100s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)
    print('Logging %s to console' % (FLAGS.console_log_level))


def maintenance_setup(client, robot):
    # function for maintenance will be called later on
    try:
        maintenance = robot.generate_maintenance(client, client._private_key)
        set = robot.generate_cleared_maintenance(client, client._private_key)
        note = robot.create_note()

        types = client.get_maintenance_type(robot.data.device_id.serial_number)
        types = types[9:-2]
        type = types.split(',')
        typeAll = []
        for typ in type:
            typeAll.append(typ)

        client.create_completed_maintenance(set)

        i = 0
        while i < len(typeAll)/2:
            clear = robot.clear_maintenance(client, client._private_key, i)
            client.clear_maintenance(clear)
            i = i + 1
        client.schedule_maintenance(maintenance, note)
    except Exception as e:
        print 'maintenance' + str(e)


'''
# doesn't work right now - status code is always 400
def create_robot(client, serial, robot_type):
    """ Publishes device metadata to create a new device """
    if robot_type is not 'random':
        logging.info('Only random robot types are currently supported. Skipping asset.')
        return None

    else:
        metadata = mock_robot.random_controller(serial)
        status_code = client.post_robotdata(serial, metadata)

    if status_code is not 200:
        logging.info('Could not create robot {} (error: {}). Skipping asset.'.format(serial, status_code))
        exit()
        return None
    exit()
    return
'''


def configure_for_tier():
    global MUST_INCLUDE
    global TIER
    try:
        cluster = re.match('name=(\S+)', os.environ['CLUSTER_PROPERTIES']).group(1)
        if 'dev' in cluster:
            TIER = 'dev'
        elif 'prod' in cluster:
            TIER = 'prod'
    except KeyError:
        TIER = None

    logging.info("Tier: {}".format(TIER))

    MUST_INCLUDE = []

    if FLAGS.mock_robot is True:
        MUST_INCLUDE += ['VF' + str(sn).zfill(4) for sn in xrange(1, 5)]
        MUST_INCLUDE += ['I' + str(sn).zfill(4) for sn in xrange(1, 5)]
        MUST_INCLUDE += ['PDM' + str(sn).zfill(4) for sn in xrange(1, 5)]

    if TIER is 'prod':
        FLAGS.bot = 'Prod'
        FLAGS.robots = 151
        FLAGS.starting_id_number = 'VF0001'
    elif TIER is 'dev' and FLAGS.mock_robot is False:
        FLAGS.bot = 'Dev'
        FLAGS.robots = 75
        FLAGS.starting_id_number = 'VF0005'
        # add Kevin's dev robots for testing
        MUST_INCLUDE += ['huawei']
    elif TIER is None:
        FLAGS.ragnar = True


def main(argv):
    try:
        argv = FLAGS(argv)
    except gflags.FlagsError as error:
        print('%s\nUsage: %s ARGS\n%s' % (error, sys.argv[0], FLAGS))
        sys.exit(1)
    configure_logging()

    if Task(FLAGS.task) == Task.PLAYBACK:
        playback_data = thing_update.get_position_data_custom()
        playback_data_iiwa = thing_update.get_position_data_sunrise()
        playback_data_agilus = thing_update.get_position_data()
        playback_data_huawei = thing_update.get_position_data_custom(log='huawei.txt')
        playback_data_joint_test = thing_update.get_position_data_custom(log='joint_test.txt')

    else:
        playback_data = None
        playback_data_iiwa = None
        playback_data_agilus = None

    pong_timeout = PONG_TIMEOUT_PER_ROBOT * FLAGS.robots

    client = kuka_connect_client.KukaConnectClient(FLAGS.json, pong_timeout)

    configure_for_tier()

    serial_numbers = FLAGS.serial_numbers.split(',')
    # generate id numbers and give them to the different robot types
    if FLAGS.generate_id_numbers is True:
        id_match = re.match(ID_REGEX, FLAGS.starting_id_number)
        id_alpha = id_match.group(1)
        id_numeric = int(id_match.group(2))
        id_numeric_min_length = len(id_match.group(2))
        serial_numbers = [id_alpha + str(id_number).zfill(id_numeric_min_length) for id_number in xrange(id_numeric, id_numeric + (FLAGS.robots))]
        for sn in MUST_INCLUDE:
            if sn not in serial_numbers:
                serial_numbers += [sn]

    # taking out the huawei robot because we have custom data for it
    try:
        serial_numbers.remove('huawei')
        serial_numbers.remove('jointtest')
    except ValueError:
        pass

    iiwas = []
    r2fs = []
    agilus = []

    for serial in serial_numbers:
        type = client.get_robotdata(serial)
        if type is None:
            logging.info("Robot {} not discovered. Skipping asset.".format(serial))
            # create_robot(client, serial, robot_type="random")
            continue
        if 'ready2_fasten' in type:
            r2fs.append(serial)
            logging.info("Found r2f")
        elif 'iiwa' in type:
            iiwas.append(serial)
            logging.info("Found iiwa")
        elif "KR" in type:
            agilus.append(serial)
            logging.info("Found KR")

    class SpecialRobot(Enum):
        READY2_FASTEN = r2fs
        CUSTOM_VARS = ['VFT0004']
        HUAWEI = ['huawei']
        JOINT_TEST = ['jointtest']

    # "robots" are ready2_fasten robots, which we are not crashing so that we gather lots of r2f data to test the app
    robots_r2f = [mock_robot_r2f.MockRobot(FLAGS.crash, False, FLAGS.maintenance, position_data=playback_data) for i in xrange(len(r2fs))]
    robots_i = [mock_robot_IIWA.MockRobot(FLAGS.crash_iiwa, FLAGS.fault, FLAGS.maintenance, position_data=playback_data_iiwa) for i in xrange(len(iiwas))]
    robots_a = [mock_robot_agilus.MockRobot(FLAGS.crash_agilus, FLAGS.fault, FLAGS.maintenance, position_data=playback_data_agilus) for i in xrange(len(agilus))]
    robot_huawei = [mock_robot_IIWA.MockRobot(False, False, False, position_data=playback_data_huawei)]
    robot_joint_test = [mock_robot_agilus.MockRobot(False, False, False, position_data=playback_data_joint_test)]

    if FLAGS.ragnar:
        ragnarbot = ragnar.Ragnar(robots_r2f, robots_i, robots_a, client, FLAGS.bot)

    # these orders are important, do not change
    # robots = robots_r2f + robots_i + robots_a
    # serials = r2fs + iiwas + agilus
    # switch to this one below to include the special robots
    robots = robots_r2f + robots_i + robots_a + robot_huawei + robot_joint_test
    serials = r2fs + iiwas + agilus + ['huawei'] + ['jointtest']

    for robot, serial in zip(robots, serials):
        robot.set_serial_number(serial)
        if serial in SpecialRobot.READY2_FASTEN.value:
            robot.add_end_effector()
            robot.set_r2f_position_data()
        if serial in SpecialRobot.CUSTOM_VARS.value:
            robot.add_test_custom_var()

    for robot, entity_id in zip(robots, serials):
        robot.set_entity_id(entity_id)

    count = 0
    num_messages = 0
    start = time.time()

# what always runs
    while(True):
        start_cycle = time.time()
        if FLAGS.ragnar:
            ragnarbot.doit()
        if FLAGS.dry_run:
            for robot in robots:
                data = robot.generate_data(FLAGS.period)
                logging.info(data.energy.energy)
        else:
            for robot in robots:
                # data
                robdata = robot.generate_data(FLAGS.period)
                client.update(robdata)
                # get nearly daily a fault when flag true
                if random.random() > 0.90 and robot.randomfaults: 
                    fault = robot.faultgenerator()
                    if fault is not None:
                        client.send_fault(fault, robot.data.device_id.serial_number)
                # get maintenance event once when flag true
                if robot.set_maintenance:
                    maintenance_setup(client, robot)
                    robot.set_maintenance = False
        num_messages += len(robots)

        delta = start_cycle - start
        if delta > 5:
            logging.info("Published %d messages at %0.2f msgs/sec" % (
                num_messages, num_messages / delta))
            num_messages = 0
            start = start_cycle

        time_to_sleep = FLAGS.period - (time.time() - start_cycle)
        if time_to_sleep > 0:
            time.sleep(time_to_sleep)
        count += 1


if __name__ == '__main__':
    main(sys.argv)
