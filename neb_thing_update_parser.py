#!/usr/bin/env python

import ast
import math
import os
import random
import re

AXES = ['A' + str(x) for x in range(1, 8)]


def get_position_data_sunrise(log='sunrise.txt', robot_id=None):

    position_data = []

    if log not in os.listdir(os.path.dirname(os.path.realpath(__file__))):
        raise ValueError('Could not find the file "{}" in my directory ({})'.format(log, os.path.dirname(os.path.realpath(__file__))))
    log_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), log)

    with open(log_path, 'r') as fd:
        content = fd.read()

    split_content = content.split('None}')
    available_robot_ids = []
    for msg in split_content:
        if 'eid' in msg:
            available_robot_id = re.search('eid(.+?),', msg).group(1)
            available_robot_id = available_robot_id[:-1]
            available_robot_id = available_robot_id[-2:]
            if available_robot_id not in available_robot_ids:
                try:
                    int(available_robot_id)
                    available_robot_ids.append(available_robot_id)
                except ValueError:
                    pass

    if not robot_id:
        robot_id = random.choice(available_robot_ids)
    elif robot_id not in available_robot_ids:
        raise ValueError('Robot id "{}" was not found in the indicated log file. Options for this file are: '.format(robot_id) + ', '.join(available_robot_ids))

    for msg in split_content:
                    try:
                        attrs = re.search('value(.+?)],', msg).group(1)
                        if "JointPositions"in attrs:
                            axis = re.search('"units": "Array", "varValue": (.+?)}}', attrs).group(1)
                            axis = axis[1:-1]
                            axisa = axis.split(',')
                            pose = []
                            for axe in axisa:
                                pose.append(math.radians(float(axe)))
                            position_data.append(pose)
                    except:
                        pass
    return position_data


def get_position_data(log='rob_data.txt', robot_id=None):
    """Parses axis positions from indicated log file (located in same directory as this file).
    Note logs should be from /var/log/nbt/fladminmgr/fladminmgr_thing_update.log from a FogNode.

    Args:
        log(str, optional): the log file to parse for axis positions
        robot_id(str, optional): the last four digits of the device EID; if blank, picks random from available

    Returns:
        position_data(list of lists): series of robot axis positions pulled from the log file
    """

    position_data = []

    if log not in os.listdir(os.path.dirname(os.path.realpath(__file__))):
        raise ValueError('Could not find the file "{}" in my directory ({})'.format(log, os.path.dirname(os.path.realpath(__file__))))
    log_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), log)

    with open(log_path, 'r') as fd:
        content = fd.read()

    split_content = content.split('None}')
    available_robot_ids = []
    for msg in split_content:
        if 'eid' in msg:
            available_robot_id = re.search('eid(.+?),', msg).group(1)
            available_robot_id = available_robot_id[:-1]
            available_robot_id = available_robot_id[-4:]
            if available_robot_id not in available_robot_ids:
                try:
                    int(available_robot_id)
                    available_robot_ids.append(available_robot_id)
                except ValueError:
                    pass

    if not robot_id:
        robot_id = random.choice(available_robot_ids)
    elif robot_id not in available_robot_ids:
        raise ValueError('Robot id "{}" was not found in the indicated log file. Options for this file are: '.format(robot_id) + ', '.join(available_robot_ids))

    # attrs
    pose = []
    # temperature = [250, 250, 250, 250, 250, 250]

    for msg in split_content:
        if 'eid' in msg:
            msg_robot_id = re.search('eid(.+?),', msg).group(1)
            msg_robot_id = msg_robot_id[:-1]
            msg_robot_id = msg_robot_id[-4:]
            if msg_robot_id == robot_id:
                pose = []
                try:
                    attrs = re.search('value(.+?)"E', msg).group(1)
                    if "AXIS_ACT" in attrs:
                        axis = re.search('A1(.+?)}', attrs).group(1) + '}'
                        axis = re.search('"value": (.+?)}', axis).group(1)
                        pose.append(math.radians(float(axis)))
                        axis = re.search('A2(.+?)}', attrs).group(1) + '}'
                        axis = re.search('"value": (.+?)}', axis).group(1)
                        pose.append(math.radians(float(axis)))
                        axis = re.search('A3(.+?)}', attrs).group(1) + '}'
                        axis = re.search('"value": (.+?)}', axis).group(1)
                        pose.append(math.radians(float(axis)))
                        axis = re.search('A4(.+?)}', attrs).group(1) + '}'
                        axis = re.search('"value": (.+?)}', axis).group(1)
                        pose.append(math.radians(float(axis)))
                        axis = re.search('A5(.+?)}', attrs).group(1) + '}'
                        axis = re.search('"value": (.+?)}', axis).group(1)
                        pose.append(math.radians(float(axis)))
                        axis = re.search('A6(.+?)}', attrs).group(1) + '}'
                        axis = re.search('"value": (.+?)}', axis).group(1)
                        pose.append(math.radians(float(axis)))
                        position_data.append(pose)
                        pose = []

                except:
                        pass
    return position_data


def get_position_data_custom(log='r2f_data.txt', robot_id=None):
    position_data = []
    log_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), log)
    with open(log_path, 'r') as fd:
        content = [list(ast.literal_eval(line)) for line in fd]
    position_data = content
    return position_data


def get_position_data_agilus(log='agilus.txt', robot_id=None):

    position_data = []

    if log not in os.listdir(os.path.dirname(os.path.realpath(__file__))):
        raise ValueError('Could not find the file "{}" in my directory ({})'.format(log, os.path.dirname(os.path.realpath(__file__))))
    log_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), log)

    with open(log_path, 'r') as fd:
        content = fd.read()

    split_content = content.split('admin')
    available_robot_ids = []
    for msg in split_content:
        if 'eid' in msg:
            available_robot_id = re.search('eid(.+?),', msg).group(1)
            available_robot_id = available_robot_id[:-1]
            available_robot_id = available_robot_id[-4:]
            if available_robot_id not in available_robot_ids:
                try:
                    int(available_robot_id)
                    available_robot_ids.append(available_robot_id)
                except ValueError:
                    pass

    if not robot_id:
        robot_id = random.choice(available_robot_ids)
    elif robot_id not in available_robot_ids:
        raise ValueError('Robot id "{}" was not found in the indicated log file. Options for this file are: '.format(robot_id) + ', '.join(available_robot_ids))

    # attrs
    pose = []
    for msg in split_content:
        try:
            attrs = re.search('value(.+?)"E', msg).group(1)
            if "AXIS_ACT_MEAS" in attrs:
                axis = re.search('A1(.+?)}', attrs).group(1) + '}'
                axis = re.search('"value": (.+?)}', axis).group(1)
                pose.append(math.radians(float(axis)))
                axis = re.search('A2(.+?)}', attrs).group(1) + '}'
                axis = re.search('"value": (.+?)}', axis).group(1)
                pose.append(math.radians(float(axis)))
                axis = re.search('A3(.+?)}', attrs).group(1) + '}'
                axis = re.search('"value": (.+?)}', axis).group(1)
                pose.append(math.radians(float(axis)))
                axis = re.search('A4(.+?)}', attrs).group(1) + '}'
                axis = re.search('"value": (.+?)}', axis).group(1)
                pose.append(math.radians(float(axis)))
                axis = re.search('A5(.+?)}', attrs).group(1) + '}'
                axis = re.search('"value": (.+?)}', axis).group(1)
                pose.append(math.radians(float(axis)))
                axis = re.search('A6(.+?)}', attrs).group(1) + '}'
                axis = re.search('"value": (.+?)}', axis).group(1)
                pose.append(math.radians(float(axis)))
                position_data.append(pose)
                pose = []

        except:
            pass

    return position_data


if __name__ == '__main__':
    get_position_data()
