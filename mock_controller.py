MOCK_VARIABLES = {
    'ROBOT_TYPE': 'SAINT',
    'SERIAL_NUMBER': '5',
    'ROBOT_NAME': 'Johnny',
    'CUSTOMER_NAME': 'NOVA Laboratories',
    'VERSION': '1.0',
    'NUMBER_AXES': '24',
    'MODE': 'Alive',
    'RUN_TIME': '720',
}


class MockController(object):

    def __init__(self):
        pass

    def connect(self):
        pass

    def readAll(self):
        return {v: self.read(v) for v in MOCK_VARIABLES}

    def read(self, variable):
        if variable in MOCK_VARIABLES:
            return MOCK_VARIABLES[variable]
        return None
