#!/usr/bin/python

import gflags
import sys
import websocket

FLAGS = gflags.FLAGS
gflags.DEFINE_string('server', 'www.kuka-atx.com/api/deviceEvents',
                     'Address of the Webocket server.')
gflags.DEFINE_string('protocol', 'wss', '"ws" or "wss" for insecure and secure sockets respectively.')
gflags.DEFINE_string('serial_number', '5', 'Serial number of robot to monitor.')
gflags.DEFINE_bool('debug', False, 'Turn on verbose debugging messages.')


def build_event_url():
    return '%s://%s/eventsOut/json/%s' % (
            FLAGS.protocol, FLAGS.server, FLAGS.serial_number)


def build_ops_url():
    return '%s://%s/operationalDataOut/json/%s' % (
            FLAGS.protocol, FLAGS.server, FLAGS.serial_number)


def on_message(ws, message):
    print 'Received', message


def on_open(ws):
    if FLAGS.debug:
        print 'Opened'


def main(argv):
    try:
        argv = FLAGS(argv)
    except gflags.FlagsError, e:
        print '%s\nUsage: %s ARGS\n%s' % (e, sys.argv[0], FLAGS)
        sys.exit(1)

    if FLAGS.debug:
        websocket.enableTrace(True)

    url = build_ops_url()
    print 'Connecting to', url
    ws = websocket.WebSocketApp(url)
    ws.on_open = on_open
    ws.on_message = on_message

    ws.run_forever()


if __name__ == '__main__':
    main(sys.argv)
