#!/usr/bin/python

import collections
import dateutil.parser
import datetime
import gflags
import json
import logging
import sys
import time
import websocket

FLAGS = gflags.FLAGS
gflags.DEFINE_string('server', 'www.kuka-atx.com/api/deviceEvents',
                     'Address of the Webocket server.')
gflags.DEFINE_string('protocol', 'wss', '"ws" or "wss" for insecure and secure sockets respectively.')
gflags.DEFINE_float('event_period', 0.0,
                    'The period, in seconds, at which to publish events. Set to zero to publish no events.')
gflags.DEFINE_float('republish_period', 120.0,
                    'The period, in seconds, at which to republish data that has not changed.')
gflags.DEFINE_float('period', 1.0, 'The period, in seconds, at which to publish operational data.')
gflags.DEFINE_bool('diff_only', False, 'Publish only data that has changed.')
gflags.DEFINE_string('log_level', 'INFO', 'Logging level to send to file.')
gflags.DEFINE_string('console_log_level', 'INFO', 'Logging level to send to console.')
gflags.DEFINE_string('log_dir', '', 'Directory of log file.')
gflags.DEFINE_bool('mock', False, 'Use a mock controller and data.')

"""
To connect to a local server, print DEBUG logs and publish mock data:

  $ python ./websocket_service.py --server localhost:9000 --protocol ws --log_level DEBUG --console_log_level DEBUG --mock

To connect to kuk-atx.com and publish live data:

  $ python ./websocket_service.py

"""


def build_event_url(serial_number):
    return '%s://%s/eventsIn/json/%s' % (FLAGS.protocol, FLAGS.server, serial_number)


def build_ops_url(serial_number):
    return '%s://%s/operationalDataIn/json/%s' % (FLAGS.protocol, FLAGS.server, serial_number)


class KukaWebSocket(object):
    """Manages event and operational websocket connections."""

    def __init__(self):
        self._event_ws = None
        self._ops_ws = None
        self._serial_number = None

    def __del__(self):
        if self._event_ws is not None:
            self._event_ws.close()
            self._event_ws = None
        if self._ops_ws is not None:
            self._ops_ws.close()
            self._ops_ws = None

    def _connect(self, url, retry_attempts=3):
        """Atempt to open a websocket connection to url. Returns a websocket object on success."""
        logging.info('Connecting to %s', (url))
        for i in xrange(retry_attempts):
            try:
                ws = websocket.WebSocket()
                ws.connect(url)
                return ws
            except Exception as e:
                logging.warning('Unable to connect to %s (attempt %d): %s', url, i, str(e))
        logging.error('Unable to connect to %s after %d attempts', url, retry_attempts)
        return None

    def connect(self, serial_number):
        """Connect to vent and operational websockets."""
        self._serial_number = serial_number
        self._ops_ws = self._connect(build_ops_url(serial_number))
        self._event_ws = self._connect(build_event_url(serial_number))

    def send_event(self, event):
        """Send event data."""
        message = event.to_json()
        logging.debug('Publishing event: %s', message)
        try:
            self._event_ws.send(message)
        except Exception as e:
            logging.warning('First attempt to send message failed: %s', str(e))
            self._event_ws = self._connect(build_event_url(self._serial_number))
            try:
                self._event_ws.send(message)
            except e:
                logging.error('Second and final attempt to send message failed: %s', str(e))

    def send_operation(self, operation):
        """Send operational data."""
        message = json.dumps(operation)
        logging.debug('Publishing operation: %s', message)
        try:
            self._ops_ws.send(message)
        except Exception as e:
            logging.warning('First attempt to send message failed: %s', str(e))
            self._ops_ws = self._connect(build_ops_url(self._serial_number))
            try:
                self._ops_ws.send(message)
            except e:
                logging.error('Second and final attempt to send message failed: %s', str(e))

    def close(self):
        logging.info('Closing websockets')
        self._event_ws.close()
        self._event_ws = None
        self._ops_ws.close()
        self._ops_ws = None


class Event(object):
    def __init__(self):
        self.id = 42
        self.code = 'Broken robot'
        self.timestamp = datetime.datetime.now()
        self.cleared = False

    def from_json(self, text):
        for key, value in json.loads(text).iteritems():
            if key == 'faultId':
                self.id = int(value)
            elif key == 'faultCode':
                self.code = value
            elif key == 'faultDateTime':
                self.datetime = dateutil.parser.parse(value)
            elif key == 'isCleared':
                self.cleared = value
            else:
                logging.warn('Unrecognized key "%s" when parsing json Event.', key)

    def to_json(self):
        return json.dumps({
            'faultId': self.id,
            'faultCode': self.code,
            'faultDateTime': self.timestamp.isoformat(),
            'isCleared': self.cleared})


def strip_timestamp(value):
    """Strips the third entry in the tuple value."""
    if value is not None and len(value) == 3:
        return value[:2]


def configure_logging():
    """Configures logging from command line flags."""
    timestr = time.strftime("%Y%m%d_%H%M%S")
    log_path = '%swebsocket_service_%s.log' % (FLAGS.log_dir, timestr)
    logging.basicConfig(filename=log_path, level=FLAGS.log_level)
    print 'Logging %s to %s' % (FLAGS.log_level, log_path)

    # Print to console.
    # TODO(charlesd): Let console log messages at a lower level than file.
    console = logging.StreamHandler()
    console.setLevel(FLAGS.console_log_level)
    formatter = logging.Formatter('%(levelname)-8s %(message).100s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)
    print 'Logging %s to console' % (FLAGS.console_log_level)

    if FLAGS.console_log_level == 'DEBUG':
        websocket.enableTrace(True)


def main(argv):
    try:
        argv = FLAGS(argv)
    except gflags.FlagsError, e:
        print '%s\nUsage: %s ARGS\n%s' % (e, sys.argv[0], FLAGS)
        sys.exit(1)

    configure_logging()

    if FLAGS.mock:
        import mock_controller
        robot = mock_controller.MockController()
    else:
        import controller
        robot = controller.Controller()

    robot.connect()
    serial_number = robot.read('SERIAL_NUMBER')

    ws = KukaWebSocket()
    ws.connect(serial_number)

    event_rate = 0
    if FLAGS.event_period > 0.001:
        event_rate = int(FLAGS.event_period / FLAGS.period)

    republish_rate = 0
    if FLAGS.republish_period > 0.001:
        republish_rate = int(FLAGS.republish_period / FLAGS.period)

    robot_state = collections.defaultdict(lambda: None)
    count = 0
    event = Event()
    event.cleared = True
    while True:
        # Publish operational data.
        for variable, value in robot.readAll().iteritems():
            if FLAGS.diff_only:
                stripped_value = strip_timestamp(value)
                if stripped_value != robot_state[variable]:
                    robot_state[variable] = stripped_value
                    ws.send_operation({variable: value})
            else:
                ws.send_operation({variable: value})
        count += 1

        # Publish an event.
        if event_rate > 0 and count % event_rate == 0:
            event.id += 1
            event.timestamp = datetime.datetime.now()
            event.cleared = not event.cleared
            ws.send_event(event)

        # Purge robot_state so all variables will be republished.
        if republish_rate > 0 and count % republish_rate == 0:
            logging.debug('Clearing robot state.')
            robot_state = collections.defaultdict(lambda: None)

        time.sleep(FLAGS.period)


if __name__ == '__main__':
    main(sys.argv)
