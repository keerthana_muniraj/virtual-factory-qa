FROM python:2.7

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY kuka /usr/src/app/kuka
COPY certificates /usr/src/app/certificates
COPY *.py /usr/src/app/
COPY *.txt /usr/src/app/


CMD ["python", "/usr/src/app/kuka_test.py", \
    "--server", "operational-data.default.svc.cluster.local:9000", \
    "--ws_protocol", "ws", \
    "--task", "playback", \
    "--crash", "true",\
    "--robots", "3", \
    "--period", "1", \
    "--starting_id_number", "7", \
    "--generate_id_numbers", "true" ]
